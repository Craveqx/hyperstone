﻿using Domain;
using Logic;
using Logic.DataStorage;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NLog;

namespace Haiper.Controllers
{
    /// <summary>
    /// Контроллер постов.
    /// </summary>
    [Route("api/[controller]")]
    public class PostController : Controller
    {
        private readonly IPostRepository postRepository;
        private readonly IPublishScheduler publishScheduler;

        private readonly ILogger<PostController> logger;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="postRepository">Репозиторий постов.</param>
        /// <param name="publishScheduler">Планировщик постинга.</param>
        /// <param name="logger">Логгер.</param>
        public PostController(IPostRepository postRepository,
            IPublishScheduler publishScheduler, 
            ILogger<PostController> logger)
        {
            this.postRepository = postRepository;
            this.publishScheduler = publishScheduler;
            this.logger = logger;
        }
        
        /// <summary>
        /// Возвращает список постов.
        /// </summary>
        /// <returns><see cref="IActionResult"/>.</returns>
        [HttpGet]
        public IActionResult Get()
        {
            return postRepository
                .GetAll()
                .ToObjectResult();
        }


        /// <summary>
        /// Возвращает пост по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор.</param>
        /// <returns>Результат.</returns>
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var post = postRepository.Get(id);
            if (post == null)
            {
                return NotFound();
            }
            return new ObjectResult(post);
        }

        /// <summary>
        /// Ставить пост на публикацию.
        /// </summary>
        /// <param name="post">Пост.</param>
        /// <returns>Результат действия.</returns>
        [HttpPost]
        public ObjectResult Post([FromBody] VkPost post)
        {
            return publishScheduler
                .SchedulePost(post)
                .ToObjectResult();
        }

        /// <summary>
        /// Обновляет пост.
        /// </summary>
        /// <param name="id">идентификатор поста.</param>
        /// <param name="post">Содержимое поста.</param>
        /// <returns>Результат действия.</returns>
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] VkPost post)
        {
            var storedPost = postRepository.Get(id);
            if (storedPost == null)
                return NotFound();
            storedPost.CopyFrom(post);
            return publishScheduler
                .UpdatePost(storedPost)
                .ToObjectResult();
        }

        /// <summary>
        /// Удаляет пост.
        /// </summary>
        /// <param name="id">Идентификатор поста.</param>
        /// <returns>Результат дейтсвиф.</returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var storedPost = postRepository.Get(id);
            if (storedPost == null)
            {
                return NotFound();
            }
            return publishScheduler
                .RemovePost(storedPost)
                .ToObjectResult();
        }
    }
}