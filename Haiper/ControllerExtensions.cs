﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Microsoft.AspNetCore.Mvc;

namespace Haiper
{
    public  static class ControllerExtensions
    {
        public static ObjectResult  ToObjectResult<T>(this Result<T> result)
        {
            if (result.IsFailure)
                return
                    new ObjectResult(new
                    {
                        result.Error
                    })
                    {
                        StatusCode = (int) HttpStatusCode.InternalServerError
                    };

            return new OkObjectResult(result.Value);
        }


        public static ObjectResult ToObjectResult(this Result result)
        {
            if (result.IsFailure)
                return
                    new ObjectResult(new
                    {
                        result.Error
                    })
                    {
                        StatusCode = (int)HttpStatusCode.InternalServerError
                    };

            return new OkObjectResult(new{});
        }
    }
}
