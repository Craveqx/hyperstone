﻿using System;

namespace Domain
{
    /// <summary>
    ///     Пост в ВК.
    /// </summary>
    public class VkPost
    {
        /// <summary>
        ///     Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Заголовок.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///     Содержимое поста.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        ///     Дата публикации.
        /// </summary>
        public DateTime PublishDate { get; set; } = DateTime.Now;

        /// <summary>
        ///     Дата изменения.
        /// </summary>
        public DateTime? ChangeDate { get; set; }

        /// <summary>
        ///     Имя пользователя.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Статус поста.
        /// </summary>
        public VkPostStatus Status { get; set; }
        
        /// <summary>
        /// Идентификатор группы (отрицательное число) или пользователя.
        /// </summary>
        public string OwnerId {get;set;}

        /// <summary>
        /// Копирует свойства объекта из другого объекта.
        /// </summary>
        /// <param name="post">Пост.</param>
        public void CopyFrom(VkPost post)
        {
            Title = post.Title;
            Content = post.Content;
            ChangeDate = new DateTime();
        }
    }
}