﻿namespace Domain
{
    /// <summary>
    /// Статусы для постов.
    /// </summary>
    public enum VkPostStatus
    {
        /// <summary>
        /// Статус не определен.
        /// </summary>
        None =0,

        /// <summary>
        /// черновик.
        /// </summary>
        Draft = 1,

        /// <summary>
        /// Пост запланирован.
        /// </summary>
        Scheduled = 2,

        /// <summary>
        /// Пост опубликован.
        /// </summary>
        Published = 3
    }
}