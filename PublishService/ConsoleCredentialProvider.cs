﻿using System;
using System.Net;
using System.Security.Authentication;
using Microsoft.AspNetCore.Mvc;

namespace PublishService
{
    public class ConsoleCredentialProvider 
    {
        public static NetworkCredential RequestFromUserInput()
        {
            var result = new NetworkCredential
            {
                UserName = TryGetLogin(),
                Password = TryGetPassword()
            };
            Console.WriteLine();
            return result;
        }

        private static string TryGetPassword()
        {
            while (true)
            {
                Console.Write("Enter password:");
                var password = GetPasswordAndMaskInput();
                if (!string.IsNullOrWhiteSpace(password))
                {
                    return password;
                }
                Console.WriteLine();
            }

           
        }

        private static string GetPasswordAndMaskInput()
        {
            string password = string.Empty;
            while (true)
            {
                var consoleKey = Console.ReadKey(true);
                if (consoleKey.Key == ConsoleKey.Enter)
                {
                    break;
                }
                Console.Write("*");
                password = password + consoleKey.KeyChar;

            }
            return password;
        }

        private static string TryGetLogin()
        {
            while (true)
            {
                Console.Write("Enter login for connection to vk.com : ");
                var result = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(result))
                {
                    return result;
                }
                Console.WriteLine();
            }
        }
    }
}