﻿using System;
using Logic.DataStorage;
using Logic.VkApi;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;

namespace PublishService
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Run posting service");
            var servicesProvider = BuildDi();
            var runner = servicesProvider.GetRequiredService<PostPublisherRunner>();

            runner.Start();
            Console.ReadLine();
        }

        private static IServiceProvider BuildDi()
        {
            var services = new ServiceCollection();

            //Runner is the custom class
            services.AddTransient<PostPublisherRunner>();

            services.AddSingleton<ILoggerFactory, LoggerFactory>();
            services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));
            services.AddSingleton< IPostRepository, LiteDbPostRepository>();
            services.AddSingleton<IVkPostClient, VkPostApiClient>();
            services.AddSingleton<IVkTokenProvider, VkTokenProvider>();

            var serviceProvider = services.BuildServiceProvider();

            var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();

            //configure NLog
            loggerFactory.AddNLog(new NLogProviderOptions { CaptureMessageTemplates = true, CaptureMessageProperties = true });
            loggerFactory.ConfigureNLog("nlog.config");

            return serviceProvider;
        }
    }
}
