﻿using System;
using System.Net;
using System.Security.Authentication;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Domain;
using Logic.DataStorage;
using Logic.VkApi;
using Microsoft.Extensions.Logging;

namespace PublishService
{
    public class PostPublisherRunner
    {
        private const int ProcessSleepSeconds = 30;
        private readonly IPostRepository postRepository;
        private readonly IVkTokenProvider vkTokenProvider;

        private readonly ILogger<PostPublisherRunner> logger;
        private readonly IVkPostClient vkPostClient;
        private  string token;

        public PostPublisherRunner(IPostRepository postRepository, 
            IVkTokenProvider vkTokenProvider,
            ILogger<PostPublisherRunner> logger,
             IVkPostClient vkPostClient)
        {
            this.postRepository = postRepository;
            this.vkTokenProvider = vkTokenProvider;
            this.logger = logger;
            this.vkPostClient = vkPostClient;
        }

        public async void Start()
        {
            var credentials = ConsoleCredentialProvider.RequestFromUserInput();
            await vkTokenProvider.GetAccessToken(credentials)
                .OnSuccess(x =>
                {
                    token = x;
                    Console.WriteLine($"Login successfull. Token = {x}");
                    return RunPublishProcess();
                })
                .OnFailure(x => Console.WriteLine("Login failed"));
            
        }

        private async Task RunPublishProcess()
        {
            Console.WriteLine("Publish process started ...");
            while (true)
            {
                Console.WriteLine("Get post scheduled for publish");
                var postsForPublish = postRepository.GetPostsReadyForPublish();
                if (postsForPublish.IsFailure)
                {
                    logger.LogError(postsForPublish.Error);
                }
                else
                {
                    Console.WriteLine($"Processing {postsForPublish.Value.Count} posts");
                    foreach (var post in postsForPublish.Value)
                    {
                        await (PublishPost(post))
                            .OnSuccess(() => logger.LogInformation($"Пост с идентификатором {post.Id} успешно опубликован."))
                            .OnFailure(err => logger.LogError(err));
                    }
                }
                Console.WriteLine($"Proccessing done sleep for {ProcessSleepSeconds} seconds");
                await Task.Delay(TimeSpan.FromSeconds(ProcessSleepSeconds));
            }
        }



        private  Task<Result> PublishPost(VkPost vkPost)
        {
            return  vkPostClient.Publish(vkPost, token)
                .OnFailure(x => logger.LogError($"Error on publish post : {x}"));

        }
    }
}