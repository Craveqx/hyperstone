﻿using System;
using System.Collections.Generic;
using System.Linq;
using CSharpFunctionalExtensions;
using Domain;
using LiteDB;
using Microsoft.Extensions.Logging;

namespace Logic.DataStorage
{
    /// <summary>
    /// Класс управляет хранилищем постов в LiteDb.
    /// </summary>
    public class LiteDbPostRepository: IPostRepository
    {
        private const string LiteDbPath = "E:\\Hyperstone\\posts.db";
        private const string PostCollection = "posts";
        private readonly ILogger<LiteDbPostRepository> logger;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="logger"></param>
        public LiteDbPostRepository(ILogger<LiteDbPostRepository> logger)
        {
            this.logger = logger;
        }

        /// <inheritdoc />
        public Result<int> Add(VkPost post)
        {
            using (var db = new LiteDatabase(LiteDbPath))
            {
                try
                {
                    var collection = db.GetCollection<VkPost>(PostCollection);
                    var id = collection.Insert(post);
                    return Result.Ok(id.AsInt32);
                }
                catch (Exception ex)
                {
                    var error = $"Ошибка при добавлении поста в хранилище. Error: \n{ex.Message}";
                    logger.LogError(ex, error);
                    return Result.Fail<int>(error);
                }
            }
        }

        /// <inheritdoc />
        public Result Update(VkPost post)
        {
            using (var db = new LiteDatabase(LiteDbPath))
            {
                try
                {
                    var collection = db.GetCollection<VkPost>(PostCollection);
                    var isUpdated = collection.Update(post);
                    if (isUpdated)
                    {
                        return Result.Ok();
                    }
                    var message = $"Пост с идентификатором {post.Id} не найден";
                    logger.LogError(message);
                    return Result.Fail(message);
                }
                catch (Exception ex)
                {
                    var error = $"Ошибка при добавлении поста в хранилище. Error: \n{ex.Message}";
                    logger.LogError(ex, error);
                    return Result.Fail<int>(error);
                }
            }
        }

        /// <inheritdoc />
        public Result Delete(VkPost post)
        {
            using (var db = new LiteDatabase(LiteDbPath))
            {
                try
                {
                    var collection = db.GetCollection<VkPost>(PostCollection);
                    var isUpdated = collection.Delete(post.Id);
                    if (isUpdated)
                    {
                        return Result.Ok();
                    }
                    var message = $"Пост с идентификатором {post.Id} не найден";
                    logger.LogError(message);
                    return Result.Fail(message);
                }
                catch (Exception ex)
                {
                    var error = $"Ошибка при удаления поста из хранилища. Error: \n{ex.Message}";
                    logger.LogError(ex, error);
                    return Result.Fail<int>(error);
                }
            }
        }

        /// <inheritdoc />
        public Result<List<VkPost>> GetAll()
        {
            using (var db = new LiteDatabase(LiteDbPath))
            {
                try
                {
                    var collection = db.GetCollection<VkPost>(PostCollection);
                    return Result.Ok(collection.FindAll().ToList());
                }
                catch (Exception ex)
                {
                    var error = $"Ошибка при получении списка постов из хранилища. Error: \n{ex.Message}";
                    logger.LogError(ex, error);
                    return Result.Fail<List<VkPost>>(error);
                }
            }
        }

        /// <inheritdoc />
        public VkPost Get(int id)
        {
            using (var db = new LiteDatabase(LiteDbPath))
            {
                try
                {
                    var collection = db.GetCollection<VkPost>(PostCollection);
                    return  collection.FindById(id);
                }
                catch (Exception ex)
                {
                    var error = $"Ошибка при получении списка постов из хранилища. Error: \n{ex.Message}";
                    logger.LogError(ex, error);
                    throw;
                }
            }
        }

        /// <inheritdoc />
        public Result<List<VkPost>> GetPostsReadyForPublish()
        {
            using (var db = new LiteDatabase(LiteDbPath))
            {
                try
                {
                    var collection = db.GetCollection<VkPost>(PostCollection);
                    var query1 = Query.EQ("Status", new BsonValue(VkPostStatus.Scheduled.ToString("G")));
                    var query2 = Query.LT("PublishDate", new BsonValue(DateTime.Now));
                    var vkPosts = collection
                        .Find(Query.And(query1,query2))
                        .ToList();
                    return Result.Ok(vkPosts);
                }
                catch (Exception ex)
                {
                    var error = $"Ошибка при получении списка постов из хранилища. Error: \n{ex.Message}";
                    logger.LogError(ex, error);
                    return Result.Fail<List<VkPost>>(error);
                }
            }
        }
    }
}