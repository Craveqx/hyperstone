﻿using System.Collections.Generic;
using CSharpFunctionalExtensions;
using Domain;

namespace Logic.DataStorage
{
    /// <summary>
    /// Интефрейс репозитория постов.
    /// </summary>
    public interface IPostRepository
    {
        /// <summary>
        /// Добавляет пост.
        /// </summary>
        /// <param name="post">Пост.</param>
        /// <returns>Результат добавления.</returns>
        Result<int> Add(VkPost post);
        /// <summary>
        /// Обновляет пост в репозитории.
        /// </summary>
        /// <param name="post">Пост.</param>
        /// <returns>Результат операции.</returns>
        Result Update(VkPost post);

        /// <summary>
        /// Удаляет пост.
        /// </summary>
        /// <param name="post">Пост.</param>
        /// <returns>Результат операции.</returns>
        Result Delete(VkPost post);

        /// <summary>
        /// Возвращает все посты.
        /// </summary>
        /// <returns>Результат и список постов.</returns>
        Result<List<VkPost>> GetAll();

        /// <summary>
        /// Возвращает пост по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор.</param>
        /// <returns>Результат с постом.</returns>
        VkPost Get(int id);

        /// <summary>
        /// Возвращает  посты готовые к публикации.
        /// </summary>
        /// <returns>Список постов.</returns>
        Result<List<VkPost>> GetPostsReadyForPublish();
    }
}