﻿using System.Net;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;

namespace Logic.VkApi
{
    public interface IVkTokenProvider
    {
        Task<Result<string>> GetAccessToken(NetworkCredential credential);
    }
}