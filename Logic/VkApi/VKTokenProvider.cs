﻿using System;
using System.Net;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Microsoft.Extensions.Logging;

namespace Logic.VkApi
{
    /// <summary>
    /// Провайдер токена к API запросам.
    /// </summary>
    public sealed class VkTokenProvider : IVkTokenProvider
    {
        private readonly ILogger<VkPostApiClient> logger;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="logger">Логер.</param>
        public VkTokenProvider(ILogger<VkPostApiClient> logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Возвращает токен доступа.
        /// </summary>
        /// <param name="credential">Учетные данные.</param>
        /// <returns>Токен.</returns>
        public async Task<Result<string>> GetAccessToken(NetworkCredential credential)
        {
            try
            {
                var loginResult = await VkAuthHandler.Login(credential);
                if (loginResult.IsFailure)
                {
                    var message = $"Error on authorized in vk: {loginResult.Error}";
                    logger.LogError(message);
                    Result.Fail(message);
                }
                return loginResult;
            }
            catch (Exception e)
            {
                var messge = "Unexpected exception on authorization";
                logger.LogError(e, $"{messge}");
                return Result.Fail<string>(messge);
            }
        }
    }
}