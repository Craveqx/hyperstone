﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Domain;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace Logic.VkApi
{
    /// <summary>
    /// Клиента для управления постами в вк.
    /// </summary>
    public class VkPostApiClient : IVkPostClient
    {
        private readonly ILogger<VkPostApiClient> logger;
        private const string VkbaseApiUrl = "https://api.vk.com/method";
        private string accessToken;

        public VkPostApiClient(ILogger<VkPostApiClient> logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Публикация поста в вк.
        /// </summary>
        /// <param name="post">Пост.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        public async Task<Result> Publish(VkPost post, string token)
        {
            HttpClient httpClient = new HttpClient();
            var url = $"{VkbaseApiUrl}/wall.post";
            var nameValueCollection = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("access_token", token),
                new KeyValuePair<string, string>("owner_id", post.OwnerId),
                new KeyValuePair<string, string>("message", post.Content),
                new KeyValuePair<string, string>("from_group", "1"),
                new KeyValuePair<string, string>("v", "5.73")
            };
            var content = new FormUrlEncodedContent(nameValueCollection);
            try
            {

                var result = await httpClient.PostAsync(url, content);
                result.EnsureSuccessStatusCode();
                var response = await result.Content.ReadAsStringAsync();
                Console.WriteLine(response);
                return  Result.Ok();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                var message = $"Failed publish post with owner {post.OwnerId} from user {post.UserName}";
                logger.LogError(message, e);
                return  Result.Fail(message);
            }
        }

        /// <summary>
        /// Удаление поста.
        /// </summary>
        /// <param name="post">Пост.</param>
        /// <returns>Результат.</returns>
        public Result Delete(VkPost post, string token)
        {
            throw new System.NotImplementedException();
        }
    }
}