﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using HtmlAgilityPack;

namespace Logic.VkApi
{
    public class VkAuthHandler
    {
        private const string AuthRequestUri =
                "https://oauth.vk.com/authorize?client_id=6033143revoke=0&display=mobile&redirect_uri=https://oauth.vk.com/blank.html&response_type=token";
        private const string UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0";
        private const string AccessToken = "access_token";

        public static async Task<Result<string>> Login(NetworkCredential clientCredential)
        {
            using (var httpClient = CreateHttpClientWithCookies())
            {
                var authRequest = await httpClient.GetAsync(AuthRequestUri);
                authRequest.EnsureSuccessStatusCode();
                var htmlContent = await authRequest.Content.ReadAsStreamAsync();

                var doc = new HtmlDocument();
                doc.Load(htmlContent);
                var formDataDictionary = ParseDocumentAndGetPostDictionary(doc);
                AddUserCredentials(formDataDictionary, clientCredential);
                var postUrl = ParseDocAndGetFormUrl(doc);
                var authFormPostResponse = await httpClient.PostAsync(postUrl, new FormUrlEncodedContent(formDataDictionary));
                if (authFormPostResponse.StatusCode == HttpStatusCode.Found)
                {
                    var grantAccessResponse = await httpClient.GetAsync(authFormPostResponse.Headers.Location);
                    if (grantAccessResponse.StatusCode == HttpStatusCode.Found)
                    {
                        var grantAccessResult = await httpClient.GetAsync(grantAccessResponse.Headers.Location);
                        if (grantAccessResult.Headers.Location.ToString().Contains(AccessToken))
                        {
                            return ParseAccessTokenFromUrl(grantAccessResult.Headers.Location);
                        }
                    }
                    return Result.Fail<string>("Grant access response StatusCode not 302");
                }
                return Result.Fail<string>("Auth post response StatusCode not 302");
            }
        }

        private static Result<string> ParseAccessTokenFromUrl(Uri redirectLocation)
        {
                var hashParts = redirectLocation.Fragment.Split("&");
                var accessTokenPart = hashParts.FirstOrDefault(x => x.Contains(AccessToken));
                var paramAndValue = accessTokenPart?.Split('=');
                if (paramAndValue?.Length > 1)
                    return Result.Ok(paramAndValue[1]);
                return  Result.Fail<string>("Access token not found in response url"); ;
        }

        private static void AddUserCredentials(Dictionary<string, string> dataDictionary,
            NetworkCredential clientCredential)
        {
            dataDictionary.Add("email", clientCredential.UserName);
            dataDictionary.Add("pass", clientCredential.Password);
        }

        private static string ParseDocAndGetFormUrl(HtmlDocument doc)
        {
            return doc.DocumentNode
                .SelectSingleNode("//form[@method='post']")
                .GetAttributeValue("action", "");
        }

        private static Dictionary<string, string> ParseDocumentAndGetPostDictionary(HtmlDocument doc)
        {
            return doc.DocumentNode
                .SelectNodes("//input[@type='hidden']")
                .ToDictionary(x => x.GetAttributeValue("name", ""), x => x.GetAttributeValue("value", ""));
        }

        private static HttpClient CreateHttpClientWithCookies()
        {
            var cookieContainer = new CookieContainer();
            var handler = new HttpClientHandler
            {
                CookieContainer = cookieContainer,
                AllowAutoRedirect = false
            };
            var httpClient = new HttpClient(handler);
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", UserAgent);
            return httpClient;
        }
    }
}