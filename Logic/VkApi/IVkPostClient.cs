﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Domain;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace Logic.VkApi
{
    /// <summary>
    /// Интефрейс клиента для управления постами в вк.
    /// </summary>
    public interface IVkPostClient
    {
        /// <summary>
        /// Публикация поста в вк.
        /// </summary>
        /// <param name="post">Пост.</param>
        /// <param name="token"></param>
        /// <returns>Результат.</returns>
        Task<Result> Publish(VkPost post, string token);

        /// <summary>
        /// Удаление поста.
        /// </summary>
        /// <param name="post">Пост.</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        Result Delete(VkPost post, string token);
    }
}