﻿using System;
using CSharpFunctionalExtensions;
using Domain;
using Logic.DataStorage;
using Logic.VkApi;

namespace Logic
{
    /// <summary>
    ///     Планировщик публикации поста.
    /// </summary>
    public class PublishScheduler : IPublishScheduler
    {
        private readonly IPostRepository postRepository;
        private readonly IVkPostClient vkPostClient;

        /// <summary>
        ///     Конструктор.
        /// </summary>
        /// <param name="postRepository">РЕпозиторий поста.</param>
        /// <param name="vkPostClient">Клиент для запросов в вк.</param>
        public PublishScheduler(IPostRepository postRepository, IVkPostClient vkPostClient)
        {
            this.postRepository = postRepository;
            this.vkPostClient = vkPostClient;
        }


        /// <inheritdoc />
        public Result<int> SchedulePost(VkPost post)
        {
            // TODO записать идентификатор поста в вк.
            post.Status = VkPostStatus.Scheduled;
            return postRepository.Add(post);
        }

        /// <inheritdoc />
        public Result UpdatePost(VkPost post)
        {
            if (post.PublishDate > DateTime.Now)
            {
                return Result.Fail("Пост уже опубликован");
            }
            return postRepository.Update(post);
        }

        /// <inheritdoc />
        public Result RemovePost(VkPost post)
        {
            return  postRepository.Delete(post);
        }
    }
}