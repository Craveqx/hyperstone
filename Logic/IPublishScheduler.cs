using CSharpFunctionalExtensions;
using Domain;

namespace Logic
{
    /// <summary>
    /// ��������� ����� ������������ ����������� �� ����������.
    /// </summary>
    public interface IPublishScheduler
    {
        /// <summary>
        /// ��������� ���� ��� ���������� � �����������.
        /// </summary>
        /// <param name="post">����.</param>
        /// <returns>��������� ��������.</returns>
        Result<int> SchedulePost(VkPost post);
        
        /// <summary>
        /// �������� ���� ��� ����������.
        /// </summary>
        /// <param name="post">����.</param>
        /// <returns>��������� ��������.</returns>
        Result UpdatePost(VkPost post);

        /// <summary>
        /// ������� ���� �� ������������.
        /// </summary>
        /// <param name="post">����.</param>
        /// <returns>��������� ��������.</returns>
        Result RemovePost(VkPost post);
    }
}